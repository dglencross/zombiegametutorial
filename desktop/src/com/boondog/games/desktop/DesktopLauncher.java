package com.boondog.games.desktop;

import java.util.Random;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.tools.texturepacker.TexturePacker.Settings;
import com.badlogic.gdx.tools.texturepacker.TexturePacker;
import com.boondoggames.ZombieGame;


public class DesktopLauncher {
	private static DesktopLauncher application;
	static final String assets_general = "../../assets_general/";
	static final String assets_android = "";
	
	public static void main (String[] arg) {
		// Save an instance to hand to the game. 
		if (application == null) {
			application = new DesktopLauncher();
		}
		
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		
		config.width = 1136;
		config.height = 640;
		boolean packTextures = true;
		
		Settings settings = new Settings();
        settings.maxWidth = 1024;
        settings.maxHeight = 1024;
        // These help prevent nasty anti-aliasing looking stuff
        settings.filterMin = TextureFilter.MipMapLinearNearest;
        settings.filterMag = TextureFilter.Nearest;
        
        // Padding stops weirdness from the texture filters
        settings.paddingX = 5;
        settings.paddingY = 5;
        
        
        if (packTextures) {
        	TexturePacker.process(settings, assets_general + "images", assets_android + "atlas", "main");
        	settings.duplicatePadding = true;
        	TexturePacker.process(settings, assets_general + "tiles", assets_android + "atlas", "tiles");
        }
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "zombie-game-demo";
		cfg.width = 1080;
		cfg.height = 720;
		new LwjglApplication(new ZombieGame(), config);
	}
}
