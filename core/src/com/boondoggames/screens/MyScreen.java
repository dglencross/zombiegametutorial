package com.boondoggames.screens;

import com.boondoggames.ZombieGame;

public abstract class MyScreen extends com.boondog.imports.game.MyScreen{
	public ZombieGame app;
	
	public MyScreen(ZombieGame app) {
		super(app);
		this.app = app;
	}

}
