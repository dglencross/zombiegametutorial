package com.boondoggames.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.boondoggames.ZombieGame;
import com.boondoggames.control.PlayerInputSystem;
import com.boondoggames.map.World;
import com.boondoggames.models.GameController;
import com.boondoggames.models.Tile;
import com.boondoggames.view.GameScreenRenderer;

public class MainScreen extends MyScreen {	
	// The game controller
	public GameController gameController;
	
	// Input systems
	protected PlayerInputSystem playerInputSystem;

	// Graphics
	protected GameScreenRenderer renderer;
	private TextureAtlas tilesAtlas;
	
	public MainScreen(ZombieGame arch) {
		super(arch);
		setUpGraphics();
		inputs = new InputMultiplexer();
		inputs.addProcessor(stage);
		Gdx.input.setInputProcessor(inputs);
		gameController = new GameController(this);
		renderer = new GameScreenRenderer(this);
		this.playerInputSystem = new PlayerInputSystem(this);
		gameController.createMap();
	}


	@Override
	public void render(float delta) {
		gameController.update(delta);
		renderer.update(delta);
		stage.act(delta);
		stage.draw();
		
		if (!gameController.isRunning()) {
			startGame();
		}		
	}


	@Override
	public void resize(int width, int height) {
		ZombieGame.getViewport().update(width, height);
	}

	private void setUpGraphics() {
		// Set up graphics here for passing between classes
		// Try to keep everything here to make for easy disposal at the end.
	
		tilesAtlas = new TextureAtlas("atlas/tiles.atlas");
		Tile.setSprites(tilesAtlas);
	}

	public World getWorld() {
		return gameController.getWorld();
	}

	public TextureAtlas getMainAtlas() {
		return getAssets().getAtlas("main");
	}
	
	public void startGame() {
		inputs.addProcessor(gameController.getPlayerInputSystem());
		gameController.startGame();
	}

	public PlayerInputSystem getPlayerInputSystem() {
		return playerInputSystem;
	}
	
	public InputMultiplexer getInputs() {
		return inputs;
	}

	public GameScreenRenderer getRenderer() {
		return renderer;
	}

}
