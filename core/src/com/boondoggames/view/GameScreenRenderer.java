package com.boondoggames.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.boondog.imports.game.MyScreen;
import com.boondoggames.ZombieGame;
import com.boondoggames.ai.Zombie;
import com.boondoggames.models.Bullet;
import com.boondoggames.models.Pair;
import com.boondoggames.models.PlayerCharacter;
import com.boondoggames.screens.MainScreen;

public class GameScreenRenderer {
	protected MainScreen screen;
	////////////////////////////////
	// Graphics
	////////////////////////////////
	protected SpriteBatch batch = ZombieGame.getBatch();
	private Sprite splat;
	protected BitmapFont font;

	private Color backgroundColor = new Color(0,0,0,1);

	public GameScreenRenderer(MainScreen mainScreen) {
		this.screen= mainScreen;
		splat = screen.getMainAtlas().createSprite("splat");
		splat.setSize(2, 2);

		// Load up the font
		font = screen.getAssets().getFont("fonts/TitilliumWeb-Regular.ttf", 50);
		font.setUseIntegerPositions(false);
	}

	public void update(float delta) {
		Gdx.gl.glClearColor(backgroundColor.r, backgroundColor.g, backgroundColor.b,backgroundColor.a);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		batch.setProjectionMatrix(MyScreen.getProjMatrix());
		// The order of these methods defines what's at the front (last = front)
		
		// Note: try to minimise the number of call to the sprite batch/shape renderer
		batch.begin();
		renderTiles();
		renderZombies();
		renderCharacter();
		renderBullets();
		renderText();
		batch.end();

		renderCamera();
 	}

	private void renderCamera() {
		screen.gameController.getPlayerInputSystem().updateCamera();
	}

	private void renderText() {
		font.getData().setScale(0.015f);

		font.draw(batch, getText(), screen.gameController.getPlayer().getX() - 5, screen.gameController.getPlayer().getY() + 2);
	}

	private String getText() {
		if (!getPlayerCharacter().isAlive()) {
			return "      You lost!";
		}
		if (screen.gameController.getZombiesAlive() == 0) {
			return "      You won!";
		}

		return "Zombies remaining: " + screen.gameController.getZombiesAlive() + "/" + screen.gameController.getStartingZombies();
	}
	
	private void renderTiles() {
		for (int x = 0; x < ZombieGame.getWorldWidth(); x++) {
			for (int y = 0; y < ZombieGame.getWorldHeight(); y++) {
				Pair p = screen.gameController.getWorld().getPair(x, y);
				p.draw(screen.gameController, batch);
			}
		}
	}

	private void renderBullets() {
		for(Bullet b : screen.gameController.getBullets()) {
			if (b.isLive()) {
				b.draw(batch);
			}
		}
	}

	private void renderZombies() {
		for(Zombie z : screen.gameController.getZombies()) {
			if (z.hasBeenShot()) {
				z.setSprite(splat);
			}

			z.draw(batch);
		}
	}

	private void renderCharacter() {
		if (!screen.gameController.isRunning()) {
			return;
		}

		getPlayerCharacter().draw(screen.gameController, batch);
	}

	public PlayerCharacter getPlayerCharacter() {
		return screen.gameController.getPlayer();
	}

	public void fireBullet(Zombie shot) {
		Bullet b = new Bullet(screen);

		b.fire(shot.getPos(), getPlayerCharacter());

		screen.gameController.getBullets().add(b);
	}

	public void playerCaptured() {
		getPlayerCharacter().setSprite(splat);
	}

}
