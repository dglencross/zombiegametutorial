package com.boondoggames.control;


import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.boondoggames.ZombieGame;
import com.boondoggames.ai.Zombie;
import com.boondoggames.models.Pair;
import com.boondoggames.models.PlayerCharacter;
import com.boondoggames.screens.MainScreen;

public class PlayerInputSystem implements InputProcessor {
	protected MainScreen screen;
	private Vector3 currentTouch = new Vector3();
	private float halfViewPortWidth;
	private float halfViewPortHeight;

	private OrthographicCamera camera;

	public PlayerInputSystem(MainScreen screen) {
		this.screen = screen;
		camera = (OrthographicCamera) ZombieGame.getViewport().getCamera();
		halfViewPortWidth = camera.viewportWidth / 2;
		halfViewPortHeight = camera.viewportHeight / 2;
	}

	@Override
	public boolean keyDown(int keycode) {
		if (keycode == Keys.R) {
			screen.gameController.createMap();
			screen.gameController.setRunning(true);
		}
		else if (keycode == Keys.Q) {
			screen.gameController.endGame();
		}
		
		float moveAmount = 1.0f;
		
		if (keycode == Keys.UP) {
			camera.position.y += moveAmount;
		} else if (keycode == Keys.DOWN) {
			camera.position.y -= moveAmount;
		} else if (keycode == Keys.LEFT) {
			camera.position.x -= moveAmount;
		} else if (keycode == Keys.RIGHT) {
			camera.position.x += moveAmount;
		} else if (keycode == Keys.Z) {
			camera.zoom += moveAmount;
		} else if (keycode == Keys.X) {
			camera.zoom -= moveAmount;
		}
		
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	public void updateCamera() {

		if (camera == null || camera.position == null || getPlayer() == null) {
			return;
		}

		float x = getPlayer().getX();
		float y = getPlayer().getY();

		if (x < halfViewPortWidth) {
			camera.position.x = halfViewPortWidth;
		} else if (x > ZombieGame.getWorldWidth() - halfViewPortWidth) {
			camera.position.x = ZombieGame.getWorldWidth() - halfViewPortWidth;
		} else {
			camera.position.x = x;
		}

		if (y < halfViewPortHeight) {
			camera.position.y = halfViewPortHeight;
		} else if (y > ZombieGame.getWorldHeight() - halfViewPortHeight) {
			camera.position.y = ZombieGame.getWorldHeight() - halfViewPortHeight;
		} else {
			camera.position.y = y;
		}

	}

	private PlayerCharacter getPlayer() {
		return screen.getRenderer().getPlayerCharacter();
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {

		if (!screen.gameController.isRunning()) {
			return false;
		}

		currentTouch.x = screenX;
		currentTouch.y = screenY;

		convertScreenToWorldCoordinates(currentTouch);

		int x = (int) currentTouch.x;
		int y = (int) currentTouch.y;

		Pair selectedTile = screen.gameController.getWorld().getMap()[x][y];
		/*
		 *
		 * Pretty much all commands are done here...
		 *
		 */

		Zombie shot = getTouchedZombie(x, y);

		if (shot != null) {
			getPlayer().shootZombie(shot);
			return false;
		}

		getPlayer().setDestination(selectedTile);

		return false;

	}

	private Zombie getTouchedZombie(int x, int y) {
		Vector2 touch = new Vector2(x,y);
		for (Zombie z : screen.gameController.getZombies()) {
			if (!z.hasBeenShot() && z.getPos().dst(touch) < 2) {
				return z;
			}
		}

		return null;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	private Vector3 convertScreenToWorldCoordinates(Vector3 touch) {
		ZombieGame.getViewport().unproject(touch);

		if (touch.x < 0 ){
			touch.x = 0;
		} else if (touch.x > screen.gameController.getWorld().getWidth()) {
			touch.x = screen.gameController.getWorld().getWidth();
		}

		if (touch.y < 0) {
			touch.y = 0;
		} else if (touch.y > screen.gameController.getWorld().getHeight()) {
			touch.y = screen.gameController.getWorld().getHeight();
		}


		return touch;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}
}
