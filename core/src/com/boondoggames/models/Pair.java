package com.boondoggames.models;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.boondoggames.map.World;

public class Pair {
	public final int x, y;
	public boolean isLand;

	public static final int WATERLEVEL = Integer.MAX_VALUE;

	// Pathfinding stuff.
	boolean pass = true;
	public Pair parent = null;
	public float g,h,f;  //g = from start; h = to end, f = both together
	Tile tile;
	
	public Pair(int x, int y, boolean isLand) {
		this.x = x;
		this.y = y;
		this.isLand = isLand;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Pair)) {
			return false;
		}
		
		if (obj == this) {
			return true;
		}
		
		Pair pairObj = (Pair)obj;
		
		if (pairObj.x == this.x && pairObj.y == this.y) {
			return true;
		}
		
		return super.equals(obj);
	}
	
	@Override
	public int hashCode() {
		int hash = 7;
		hash = 31 * hash + x;
		hash = 31 * hash + y;
		return hash;
	}

	public boolean isWater() {
		return !isLand;
	}

	public int getLevel() {
		if (isWater()) {
			return WATERLEVEL;
		}
		return -1;
	}

	public Vector2 asVector() {
		return new Vector2(x, y);
	}

	public void setupTile() {
		tile = new Tile();
	}

	public void draw(GameController game, SpriteBatch batch) {
		tile.draw(game, batch,this);
	}

	/* Pathfinding methods */

	public void resetPathStuff() {
		g = 0;
		f = 0;
		h = 0;
		parent = null;
		pass = true;
	}

	public void updateGHFP(float g, float h, Pair parent){
		this.parent = parent;
		this.g = g;
		this.h = h;
		f = g+h;
	}
}
