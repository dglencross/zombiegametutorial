package com.boondoggames.models;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.boondoggames.ai.Zombie;
import com.boondoggames.jumppointsearch.JPS;
import com.boondoggames.map.Path;
import com.boondoggames.map.Step;
import com.boondoggames.screens.MainScreen;

public class PlayerCharacter {

	private Sprite playerSprite;
	private float orientation;
	private Vector2 pos;
	private Path path;
	protected int nextPathNodeIndex = 0;
	private MainScreen screen;
	private boolean isAlive = true;

	private Vector2 currentPN = new Vector2(), nextPN = new Vector2(), moveVec = new Vector2();
	private Vector2 tmp = new Vector2();

	public PlayerCharacter(MainScreen screen) {
		playerSprite = screen.getMainAtlas().createSprite("man");
		playerSprite.setSize(2, 2);
		pos = screen.getWorld().getRandomStartPosition();
		this.screen = screen;
	}
	
	public void draw(GameController game, SpriteBatch batch) {
		playerSprite.setCenter(pos.x, pos.y);
		playerSprite.setOrigin(1, 1);
		playerSprite.setRotation(orientation);
		playerSprite.draw(batch);
	}

	public void setDestination(Pair destination) {

		if (destination.isWater()) {
			return;
		}

		pointAt(destination.asVector());

		Pair currentLocation = new Pair((int)pos.x,  (int)pos.y, true);

		JPS landJps = new JPS(screen.getWorld(), currentLocation, destination);

		path = new Path(landJps.search());
	}

	private void pointAt(Vector2 vector) {
		float angle = (float) Math.toDegrees(Math.atan2(vector.x - pos.x, vector.y - pos.y));
		this.orientation = - angle + 90f; //  + 90f
	}
	
	public float getX() {
		return pos.x;
	}
	
	public float getY() {
		return pos.y;
	}

	public void update(float delta) {
		if (null == path || path.isEmpty()) { // we have no valid path, so just return
			return;
		}

		pos = getNextPosition(delta);
		updateOrientation();

		if (nextPathNodeIndex > path.getLength()) { // then we've arrived
			path = null;
			nextPathNodeIndex = 0;
		}
	}

	private Vector2 getNextPosition(float delta) {
		if (nextPathNodeIndex == 0) {
			currentPN.set(stepToV2(path.getStep(0)));
			nextPathNodeIndex ++;
		} else {
			float amountToMove = 3*delta;
			do {
				nextPN.set(stepToV2(path.getStep(nextPathNodeIndex)));
				moveVec.set(nextPN);
				moveVec.sub(currentPN);

				if (moveVec.len() > amountToMove) {
					moveVec.setLength(amountToMove);
					currentPN.add(moveVec);
					amountToMove = 0;
				} else {
					amountToMove -= moveVec.len();
					currentPN.set(nextPN);
					nextPathNodeIndex++;
					if (nextPathNodeIndex>path.getLength()) {
						break;
					}
				}
			} while (amountToMove > 0);
		}
		return currentPN;
	}

	private Vector2 stepToV2(Step step) {
		return tmp.set((float)step.getX(),(float)step.getY());
	}

	private void updateOrientation() {
		if (this.nextPathNodeIndex >= this.path.getLength() - 1) {
			return;
		}

		Step step = this.path.getStep(nextPathNodeIndex);
		Vector2 nextPos = new Vector2((float)step.getX(), (float)step.getY());

		pointAt(nextPos);
	}

	public Vector2 getPos() {
		return pos;
	}

	public void shootZombie(Zombie shot) {
		screen.getRenderer().fireBullet(shot);

		pointAt(shot.getPos());

		// this stops the player character, path is cancelled when a zombie is shot
		path = null;
	}

	public void setSprite(Sprite splat) {
		playerSprite = splat;
		isAlive = false;
	}

	public boolean isAlive() {
		return isAlive;
	}
}
