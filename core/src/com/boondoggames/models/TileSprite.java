package com.boondoggames.models;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class TileSprite {
	public static final Color landColor = new Color(77/255f,219/255f,114/255f,1);
	public static final Color waterColor = new Color(77/255f,219/255f,255/255f,1);

	Integer level;

	public TileSprite(Integer level) {
		this.level = level;
	}

}
