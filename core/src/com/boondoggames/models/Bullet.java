package com.boondoggames.models;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.boondoggames.screens.MainScreen;

public class Bullet {

    private Sprite sprite;

    private Vector2 endPosition, currentPosition, dir;

    private boolean live;

    public static float BULLET_SPEED = 0.5f;

    public Bullet(MainScreen screen) {
        sprite = screen.getMainAtlas().createSprite("bullet");
        sprite.setSize(0.2f, 0.2f);
        live = true;
    }

    public void draw(SpriteBatch batch) {
        sprite.draw(batch);
    }

    public void fire(Vector2 destination, PlayerCharacter character) {
        currentPosition = character.getPos().cpy();
        endPosition = destination.cpy();

        dir = endPosition.cpy();
        dir.sub(currentPosition);
        dir.setLength(BULLET_SPEED);
    }

    public void update(float delta) {
        currentPosition.set(currentPosition).add(dir);
        sprite.setPosition(currentPosition.x, currentPosition.y);
    }

    public boolean isLive() {
        return live;
    }

    public Vector2 getPos() {
        return currentPosition;
    }

}
