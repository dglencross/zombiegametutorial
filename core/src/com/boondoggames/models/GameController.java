package com.boondoggames.models;

import com.boondoggames.ai.Zombie;
import com.boondoggames.control.PlayerInputSystem;
import com.boondoggames.map.World;
import com.boondoggames.screens.MainScreen;

import java.util.ArrayList;
import java.util.List;

public class GameController {	
	private MainScreen screen;
	private PlayerCharacter player;
	private int startingZombies = 30;
	private List<Zombie> zombies;
	private int zombiesAlive;
	private List<Bullet> bullets;
	
	// Is the game running? (not started yet, paused)
	private boolean running = false;
	
	// World, contains maps
	private World world;

	private long gameOverTime = 0;

	public GameController(MainScreen screen) {
		this.screen = screen;
	}

	public void startGame() {
		running = true;
		gameOverTime = 0;
	}
	
	public void createMap() {
		running = false;
		world = new World();
		resetPlayer();
		createZombies();
		bullets = new ArrayList<Bullet>();
	}

	private void createZombies() {
		zombies = new ArrayList<Zombie>();
		for(int i=0; i < startingZombies; i++) {
			zombies.add(new Zombie(screen));
			zombiesAlive++;
		}
	}

	public void update(float delta) {
		if (!running) {
			return;
		}

		player.update(delta);
		Zombie z = updateZombies();
		if (null != z) {
			screen.getRenderer().playerCaptured();
			z.shoot();
		}

		updateBullets(delta);
	}

	private Zombie updateZombies() {
		for(Zombie z : zombies) {
			if (z.update(screen.getRenderer().getPlayerCharacter().getPos())) {
				return z;
			}
			// check if a zombie has been hit by a bullet
			for(Bullet b : bullets) {
				if (b.isLive() && z.getPos().dst(b.getPos()) < 1.0f) {
					if (!z.hasBeenShot()) {
						zombiesAlive -= 1;
					}
					z.shoot();
				}
			}
		}
		return null;
	}

	private void updateBullets(float delta) {
		for(Bullet b : bullets) {
			b.update(delta);
		}
	}

	private void resetPlayer() {
		player = new PlayerCharacter(screen);
	}
	
	public void endGame() {
		gameOverTime = System.currentTimeMillis();
		
		screen.getInputs().removeProcessor(screen.getPlayerInputSystem());
	}

	public World getWorld() {
		return world;
	}

	public PlayerCharacter getPlayer() {
		return this.player;
	}

	public PlayerInputSystem getPlayerInputSystem() {
		return screen.getPlayerInputSystem();
	}
	
	public boolean isRunning() {
		return running;
	}

	public void setRunning(boolean b) {
		this.running = b;
	}

	public List<Zombie> getZombies() {
		return zombies;
	}

	public List<Bullet> getBullets() {
		return bullets;
	}

	public int getStartingZombies() {
		return startingZombies;
	}

	public int getZombiesAlive() {
		return zombiesAlive;
	}
	
}
