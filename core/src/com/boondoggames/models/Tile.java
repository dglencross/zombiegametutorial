package com.boondoggames.models;

import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

public class Tile {	
	static Sprite plainTile;

	public void draw(GameController game, SpriteBatch batch, Pair pair) {
		// Draw the 'base' first.
		plainTile.setColor(game.getWorld().getColorFromId(pair.getLevel()));
		plainTile.setBounds(pair.x, pair.y, 1, 1);
		plainTile.draw(batch);
	}
	
	public static void setSprites(TextureAtlas tiles) {
		plainTile = tiles.createSprite("white");
		
		// Make sure we do this, it then does it for the whole atlas.
		plainTile.getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
	}
}
