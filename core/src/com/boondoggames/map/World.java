package com.boondoggames.map;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.boondoggames.ZombieGame;
import com.boondoggames.jumppointsearch.Heap;
import com.boondoggames.models.Pair;
import com.boondoggames.models.TileSprite;

import java.util.ArrayList;
import java.util.Random;

public class World {
	/*
	 *  Contains and manages the world
	 */

	// The different types of map available
	protected Pair[][] map;

	protected Heap heap;

	// Map functions and info
	protected WorldGenerator worldGen;

	public World() {
		this(true);
	}
	
	public World(boolean initiate) {

		if (initiate) {
			initiateWorld();
		}
	}

	public void initiateWorld() {
		// First, generate the land
		worldGen = new WorldGenerator();
		setMap(worldGen.getMap());
		setupTiles();
		heap = new Heap();
	}


	protected void setupTiles() {
		for (Pair[] pS : getMap()) {
			for (Pair p : pS) {
				p.setupTile();
			}
		}
	}

	public Pair[][] getMap() {
		return map;
	}
	public void setMap(Pair[][] map) {
		this.map = map;
	}

	public Pair getPair(int x, int y) {
		return map[x][y];
	}
	
	public int getWidth() {
		return ZombieGame.getWorldWidth();
	}
	
	public int getHeight() {
		return ZombieGame.getWorldHeight();
	}
	
	public Color getColorFromId(Integer level) {
		if (level == Pair.WATERLEVEL) {
			return TileSprite.waterColor;
		}  else {
			return TileSprite.landColor;
		}
	}

	public Vector2 getRandomStartPosition() {

		Random r = new Random();

		while(true) {
			Pair p = getPair(r.nextInt(getWidth()), r.nextInt(getHeight()));
			if (p.isLand) {
				return new Vector2(p.x, p.y);
			}
		}
	}

	public void resetPathStuff() {
		for (int i=0; i<map.length; i++) {
			for (int j=0; j<map[0].length; j++) {
				map[i][j].resetPathStuff();
			}
		}
	}

	//--------------------------HEAP-----------------------------------//
	/**
	 * Adds a node's (x,y,f) to the heap. The heap is sorted by 'f'.
	 *
	 * @param node (Pair) node to be added to the heap
	 */
	public void heapAdd(Pair node){
		float[] tmp = {node.x,node.y,node.f};
		heap.add(tmp);
	}

	/**
	 * @return (int) size of the heap
	 */
	public int heapSize(){
		return heap.getSize();
	}
	/**
	 * @return (Pair) takes data from popped float[] and returns the correct node
	 */
	public Pair heapPopPair(){
		float[] tmp = heap.pop();
		return getPair((int)tmp[0],(int)tmp[1]);
	}
	//-----------------------------------------------------------------//

	//---------------------------Passability------------------------------//

	//--------------------------------------------------------------------//


	public ArrayList<Pair> pathCreate(Pair node){
		ArrayList<Pair> trail = new ArrayList<Pair>();
		while (node.parent!=null){
			try{
				trail.add(0,node);
			}catch (Exception e){}
			node = node.parent;
		}
		return trail;
	}

	public float toPointApprox(float x, float y, int tx, int ty){
		return (float) Math.sqrt(Math.pow(Math.abs(x-tx),2) + Math.pow(Math.abs(y-ty), 2));
	}

	/**
	 * Tests an x,y node's passability
	 *
	 * @param x (int) node's x coordinate
	 * @param y (int) node's y coordinate
	 * @return (boolean) true if the node is obstacle free and on the map, false otherwise
	 */
	public boolean walkable(int x, int y){

		if (MapTools.outsideTheWorld(x, y, getMap())) {
			return false;
		}

		if (getMap()[x][y].isWater()) {
			return false;
		}

		return true;
	}

	/**
	 * returns all adjacent nodes that can be traversed
	 *
	 * @param node (Pair) finds the neighbors of this node
	 * @return (int[][]) list of neighbors that can be traversed
	 */
	public int[][] getNeighbors(Pair node){
		int[][] neighbors = new int[8][2];
		int x = node.x;
		int y = node.y;
		boolean d0 = false; //These booleans are for speeding up the adding of nodes.
		boolean d1 = false;
		boolean d2 = false;
		boolean d3 = false;

		if (walkable(x,y-1)){
			neighbors[0] = (tmpInt(x,y-1));
			d0 = d1 = true;
		}
		if (walkable(x+1,y)){
			neighbors[1] = (tmpInt(x+1,y));
			d1 = d2 = true;
		}
		if (walkable(x,y+1)){
			neighbors[2] = (tmpInt(x,y+1));
			d2 = d3 = true;
		}
		if (walkable(x-1,y)){
			neighbors[3] = (tmpInt(x-1,y));
			d3 = d0 = true;
		}
		if (d0 && walkable(x-1,y-1)){
			neighbors[4] = (tmpInt(x-1,y-1));
		}
		if (d1 && walkable(x+1,y-1)){
			neighbors[5] = (tmpInt(x+1,y-1));
		}
		if (d2 && walkable(x+1,y+1)){
			neighbors[6] = (tmpInt(x+1,y+1));
		}
		if (d3 && walkable(x-1,y+1)){
			neighbors[7] = (tmpInt(x-1,y+1));
		}
		return neighbors;
	}

	/**
	 * Encapsulates x,y in an int[] for returning. A helper method for the jump method
	 *
	 * @param x (int) point's x coordinate
	 * @param y (int) point's y coordinate
	 * @return ([]int) bundled x,y
	 */
	public int[] tmpInt (int x, int y){
		int[] tmpIntsTmpInt = {x,y};  //create the tmpInt's tmpInt[]
		return tmpIntsTmpInt;         //return it
	}
}
