package com.boondoggames.map;

import com.boondoggames.models.Pair;

/**
 * Created by dave on 10/12/2016.
 */

public class MapTools {
    public static boolean outsideTheWorld(int i, int j, Pair[][] map) {
        if (i < 0 || j < 0) {
            return true;
        }

        if (i >= map.length) {
            return true;
        }

        if (j >= map[0].length) {
            return true;
        }

        return false;
    }
}
