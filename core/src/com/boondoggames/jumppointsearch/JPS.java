package com.boondoggames.jumppointsearch;

import java.util.ArrayList;
import java.util.List;

import com.boondoggames.map.World;
import com.boondoggames.models.Pair;
/**
 * @author Clint Mullins
 * @referenced Javascript version of JPS by aniero / https://github.com/aniero
 */
public class JPS {
    private World grid;
    int startX,startY,endX,endY;  //variables for reference grid
    int dxMax, dyMax, dstartX, dstartY, dendX, dendY;       //variables for Large Nod
    int[] tmpXY;
    int[][] neighbors;
    float ng;
    Pair tmpPair, cur;
    Pair[] successors, possibleSuccess;
    ArrayList<Pair> trail;

    /**
     * Initializer; sets up variables, creates reference grid and actual grid, gets start and end points, initiates search
     *
     */
    public JPS(World world, Pair start, Pair end){
        this.grid = world;
        this.grid.resetPathStuff();
        this.startX = start.x;   //the start point x value
        this.startY = start.y;	  //the start point y value
        this.endX = end.x;	  //the end point x value
        this.endY = end.y;	  //the end point y value
        search();
    }

    /**
     * Orchestrates the Jump Point Search; it is explained further in comments below.
     */
    public List<Pair> search(){
        grid.getPair(startX,startY).updateGHFP(0, 0, null);
        grid.heapAdd(grid.getPair(startX, startY));  //Start Pair is added to the heap
        while (true){
            cur = grid.heapPopPair();              //the current Pair is removed from the heap.
            if (cur.x == endX && cur.y==endY){		//if the end Pair is found
                trail = grid.pathCreate(cur);    //the path is then created

                // Add the start and end points to the trail, makes things far easier down the line
                if (trail.size()>0) {
                    Pair startPair = grid.getPair(startX, startY);
                    Pair endPair = grid.getPair(endX, endY);

                    // For some reason adding a reference to the pair wont work, so we make a copy.
                    trail.add(0,new Pair(startPair.x, startPair.y, !startPair.isWater()));
                    trail.add(new Pair(endPair.x, endPair.y, !endPair.isWater()));
//					trail.add(0,startPair);
//					trail.add(endPair);

                }
                break;				//loop is done
            }
            possibleSuccess = identifySuccessors(cur);  //get all possible successors of the current Pair
            for (int i=0;i<possibleSuccess.length;i++){     //for each one of them
                if (possibleSuccess[i]!=null){				//if it is not null
                    grid.heapAdd(possibleSuccess[i]);		//add it to the heap for later use (a possible future cur)
                }
            }
            if (grid.heapSize()==0){						//if the grid size is 0, and we have not found our end, the end is unreachable
                break;										//loop is done
            }
        }

        return trail;
    }

    /**
     * returns all Pairs jumped from given Pair
     *
     * @param Pair
     * @return all Pairs jumped from given Pair
     */
    public Pair[] identifySuccessors(Pair Pair){
        successors = new Pair[8];				//empty successors list to be returned
        neighbors = getNeighborsPrune(Pair);    //all neighbors after pruned
        for (int i=0; i<neighbors.length; i++){ //for each of these neighbors
            tmpXY = jump(neighbors[i][0],neighbors[i][1],Pair.x,Pair.y); //get next jump point
            if (tmpXY[0]!=-1){								//if that point is not null( {-1,-1} )
                int x = tmpXY[0];
                int y = tmpXY[1];
                ng = (grid.toPointApprox(x,y,Pair.x,Pair.y) + Pair.g);   //get the distance from start
                if (grid.getPair(x,y).f<=0 || grid.getPair(x,y).g>ng){  //if this Pair is not already found, or we have a shorter distance from the current Pair
                    grid.getPair(x,y).updateGHFP(grid.toPointApprox(x,y,Pair.x,Pair.y)+Pair.g,grid.toPointApprox(x,y,endX,endY),Pair); //then update the rest of it
                    successors[i] = grid.getPair(x,y);  //add this Pair to the successors list to be returned
                }
            }
        }
        return successors;  //finally, successors is returned
    }

    /**
     * jump method recursively searches in the direction of parent (px,py) to child, the current Pair (x,y).
     * It will stop and return its current position in three situations:
     *
     * 1) The current Pair is the end Pair. (endX, endY)
     * 2) The current Pair is a forced neighbor.
     * 3) The current Pair is an intermediate step to a Pair that satisfies either 1) or 2)
     *
     * @param x (int) current Pair's x
     * @param y (int) current Pair's y
     * @param px (int) current.parent's x
     * @param py (int) current.parent's y
     * @return (int[]={x,y}) Pair which satisfies one of the conditions above, or null if no such Pair is found.
     */
    public int[] jump(int x, int y, int px, int py){
        int[] jx = {-1,-1}; //used to later check if full or null
        int[] jy = {-1,-1}; //used to later check if full or null
        int dx = (x-px)/Math.max(Math.abs(x-px), 1); //because parents aren't always adjacent, this is used to find parent -> child direction (for x)
        int dy = (y-py)/Math.max(Math.abs(y-py), 1); //because parents aren't always adjacent, this is used to find parent -> child direction (for y)

        if (!grid.walkable(x,y)){ //if this space is not grid.walkable, return a null.
            return tmpInt(-1,-1); //in this system, returning a {-1,-1} equates to a null and is ignored.
        }
        if (x==this.endX && y==this.endY){   //if end point, return that point. The search is over! Have a beer.
            return tmpInt(x,y);
        }
        if (dx!=0 && dy!=0){  //if x and y both changed, we are on a diagonally adjacent square: here we check for forced neighbors on diagonals
            if ((grid.walkable(x-dx,y+dy) && !grid.walkable(x-dx,y)) || //we are moving diagonally, we don't check the parent, or our next diagonal step, but the other diagonals
                    (grid.walkable(x+dx,y-dy) && !grid.walkable(x,y-dy))){  //if we find a forced neighbor here, we are on a jump point, and we return the current position
                return tmpInt(x,y);
            }
        }
        else{ //check for horizontal/vertical
            if (dx!=0){ //moving along x
                if ((grid.walkable(x+dx,y+1) && !grid.walkable(x,y+1)) || //we are moving along the x axis
                        (grid.walkable(x+dx,y-1) && !grid.walkable(x,y-1))){  //we check our side Pairs to see if they are forced neighbors
                    return tmpInt(x,y);
                }
            }
            else{
                if ((grid.walkable(x+1,y+dy) && !grid.walkable(x+1,y)) ||  //we are moving along the y axis
                        (grid.walkable(x-1,y+dy) && !grid.walkable(x-1,y))){	 //we check our side Pairs to see if they are forced neighbors
                    return tmpInt(x,y);
                }
            }
        }

        if (dx!=0 && dy!=0){ //when moving diagonally, must check for vertical/horizontal jump points
            jx = jump(x+dx,y,x,y);
            jy = jump(x,y+dy,x,y);
            if (jx[0]!=-1 || jy[0]!=-1){
                return tmpInt(x,y);
            }
        }
        if (grid.walkable(x+dx,y) || grid.walkable(x,y+dy)){ //moving diagonally, must make sure one of the vertical/horizontal neighbors is open to allow the path
            return jump(x+dx,y+dy,x,y);
        }
        else { //if we are trying to move diagonally but we are blocked by two touching corners of adjacent Pairs, we return a null
            return tmpInt(-1,-1);
        }
    }

    /**
     * Encapsulates x,y in an int[] for returning. A helper method for the jump method
     *
     * @param x (int) point's x coordinate
     * @param y (int) point's y coordinate
     * @return ([]int) bundled x,y
     */
    public int[] tmpInt (int x, int y){
        int[] tmpIntsTmpInt = {x,y};  //create the tmpInt's tmpInt[]
        return tmpIntsTmpInt;         //return it
    }

    /**
     * Returns Pairs that should be jumped based on the parent location in relation to the given Pair.
     *
     * @param Pair (Pair) Pair which has a parent (not the start Pair)
     * @return (ArrayList<Pair>) list of Pairs that will be jumped
     */
    public int[][] getNeighborsPrune(Pair Pair){
        Pair parent = Pair.parent;    //the parent Pair is retrieved for x,y values
        int x = Pair.x;
        int y = Pair.y;
        int px, py, dx, dy;
        int[][] neighbors = new int[5][2];
        //directed pruning: can ignore most neighbors, unless forced
        if (parent!=null){
            px = parent.x;
            py = parent.y;
            //get the normalized direction of travel
            dx = (x-px)/Math.max(Math.abs(x-px), 1);
            dy = (y-py)/Math.max(Math.abs(y-py), 1);
            //search diagonally
            if (dx!=0 && dy!=0){
                if (grid.walkable(x, y+dy)){
                    neighbors[0] = (tmpInt(x,y+dy));
                }
                if (grid.walkable(x+dx,y)){
                    neighbors[1] = (tmpInt(x+dx,y));
                }
                if (grid.walkable(x,y+dy) || grid.walkable(x+dx,y)){
                    neighbors[2] = (tmpInt(x+dx,y+dy));
                }
                if (!grid.walkable(x-dx,y) && grid.walkable(x,y+dy)){
                    neighbors[3] = (tmpInt(x-dx,y+dy));
                }
                if (!grid.walkable(x,y-dy) && grid.walkable(x+dx,y)){
                    neighbors[4] = (tmpInt(x+dx,y-dy));
                }
            }
            else{
                if (dx==0){
                    if (grid.walkable(x,y+dy)){
                        if (grid.walkable(x,y+dy)){
                            neighbors[0] = (tmpInt(x,y+dy));
                        }
                        if (!grid.walkable(x+1,y)){
                            neighbors[1] = (tmpInt(x+1,y+dy));
                        }
                        if (!grid.walkable(x-1,y)){
                            neighbors[2] = (tmpInt(x-1,y+dy));
                        }
                    }
                }
                else{
                    if (grid.walkable(x+dx,y)){
                        if (grid.walkable(x+dx,y)){
                            neighbors[0] = (tmpInt(x+dx,y));
                        }
                        if (!grid.walkable(x, y+1)){
                            neighbors[1] = (tmpInt(x+dx,y+1));
                        }
                        if (!grid.walkable(x, y-1)){
                            neighbors[2] = (tmpInt(x+dx,y-1));
                        }
                    }
                }
            }
        }
        else {//return all neighbors
            return grid.getNeighbors(Pair); //adds initial Pairs to be jumped from!
        }

        return neighbors; //this returns the neighbors, you know
    }
}
