package com.boondoggames.ai;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.boondoggames.models.Pair;
import com.boondoggames.screens.MainScreen;

public class Zombie {

    private Sprite sprite;
    private Vector2 pos;
    private float orientation;
    private Vector2 diff;
    private boolean hasBeenShot;
    private int attackRadius = 20;
    private MainScreen screen;

    public static float ZOMBIE_SPEED = 0.03f;

    public Zombie(MainScreen screen) {
        this.screen = screen;
        sprite = screen.getMainAtlas().createSprite("zombie");
        sprite.setSize(2, 2);

        pos = screen.getWorld().getRandomStartPosition();
        while(pos.dst(screen.gameController.getPlayer().getPos()) < attackRadius) {
            pos = screen.getWorld().getRandomStartPosition();
        }
    }

    public void draw(Batch batch) {
        sprite.setCenter(pos.x, pos.y);
        sprite.setOrigin(1, 1);
        sprite.setRotation(orientation);
        sprite.draw(batch);
    }

    private boolean withinAttackDistance(Vector2 playerPos) {
        return pos.dst(playerPos) < attackRadius;
    }

    public boolean update(Vector2 playerPos) {
        if (hasBeenShot) {
            return false;
        }

        if (withinAttackDistance(playerPos)) {
            updateOrientation(playerPos);
            moveTowardsPlayer(playerPos);
        }
        return touchingPlayer(playerPos);
    }

    private void moveTowardsPlayer(Vector2 playerPos) {
        diff = playerPos.cpy();
        diff.sub(pos);

        diff.setLength(ZOMBIE_SPEED);

        Vector2 nextPos = pos.cpy().add(diff);

        // attempt to stop zombies crossing water
        Pair nextPair = screen.getWorld().getPair((int)nextPos.x, (int)nextPos.y);
        if (nextPair.isLand) {
            pos.set(pos).add(diff);
        }

    }

    private void updateOrientation(Vector2 playerPos) {
        float angle = (float) Math.toDegrees(Math.atan2(playerPos.x - pos.x, playerPos.y - pos.y));
        this.orientation = - angle; //  + 90f
    }

    private boolean touchingPlayer(Vector2 playerPos) {
        return pos.dst(playerPos) < 0.5;
    }

    public Vector2 getPos() {
        return pos;
    }

    public void shoot() {
        hasBeenShot = true;
    }

    public boolean hasBeenShot() {
        return hasBeenShot;
    }

    public void setSprite(Sprite splat) {
        this.sprite = splat;
    }

}