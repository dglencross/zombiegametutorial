package com.boondoggames;

import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.boondog.imports.game.MyGame;
import com.boondog.imports.graphics.MyShapeRenderer;
import com.boondoggames.screens.MainScreen;

public class ZombieGame extends MyGame {
	
	static Random random = new Random();

	private static MyShapeRenderer myRend;
	
	private static int worldWidth, worldHeight;
	
	@Override
	public void create() {
		 init();
		 setScreen(new MainScreen(this));
	}
	
	@Override
	public void init() {
		super.init();
		initSingletons();	
	}

	private void initSingletons() {
		// Anything we only ever want one of, create here.
		myRend = new MyShapeRenderer();
		myRend.setCamera(getViewport().getCamera());
	}
	
	public static int getWorldWidth() {
		return worldWidth * 3;
	}
	
	public static int getWorldHeight() {		
		return worldHeight * 3;
	}

	@Override
	protected void initViewport() {
		// Total view width
		float viewWidth =32;
		float viewHeight = Math.round(Gdx.graphics.getHeight()/(Gdx.graphics.getWidth()/viewWidth));

		// World width, in squares. Always 100 wide.
		worldWidth = 27;
		worldHeight = (int) Math.floor(viewHeight);
		
		setViewport(new StretchViewport(viewWidth,viewHeight));
	}

	public static Random getRandom() {
		return random;
	}
}
