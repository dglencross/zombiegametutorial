package com.boondoggames.noise;

public interface MapNoiseGenerator {
	float[][] getMap();
}
