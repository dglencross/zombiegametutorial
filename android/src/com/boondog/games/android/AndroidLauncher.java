package com.boondog.games.android;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.boondoggames.ZombieGame;


public class AndroidLauncher extends AndroidApplication  {
	protected ZombieGame wargame;
	
	protected View gameView;
	protected RelativeLayout layout;
		
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	    layout = new RelativeLayout(this);
		// Create game
		wargame = new ZombieGame();
		
		///// Sort out layouts
		// The game 
        initGameView();

        setContentView(layout);	
	}
	
	private void initGameView() {
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		config.useWakelock = true;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);		
        
        
		gameView = initializeForView(wargame, config);

		
		// Add the view
        layout.addView(gameView);		
	}
}
